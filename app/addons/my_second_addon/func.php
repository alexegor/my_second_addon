<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

defined('BOOTSTRAP') or die('Access denied');

function fn_my_second_addon_update_product_pre(&$product_data, $product_id, $lang_code, $can_update)
{
    if (isset($product_data['release_date'])) {
        $product_data['release_date'] = fn_parse_date($product_data['release_date']);
    }
}
function fn_my_second_addon_get_products_before_select($params, $join, &$condition, $u_condition, $inventory_join_cond, $sortings, $total, $items_per_page, $lang_code, $having) 
{
    if (!empty($params['ean_code'])) {
        $ean_code = trim($params['ean_code']);
        $condition .= db_quote(" AND products.ean_code LIKE ?l", "%{$ean_code}%");
    }
    if (!empty($params['special_edition'])) {
        $special_edition = trim($params['special_edition']);
        $condition .= db_quote(" AND products.special_edition = ?s", "{$special_edition}");
    }
}
