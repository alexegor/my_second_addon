<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

defined('BOOTSTRAP') or die('Access denied');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return array(CONTROLLER_STATUS_OK);
}

if ($mode == 'manage') {

    $selected_fields = Tygh::$app['view']->getTemplateVars('selected_fields');

    $selected_fields[] = array(
        'name' => '[data][technical_description]',
        'text' => __('technical_description')
    );
    $selected_fields[] = array(
        'name' => '[data][ean_code]',
        'text' => __('my_second_addon.ean_code')
    );
    $selected_fields[] = array(
        'name' => '[data][release_date]',
        'text' => __('my_second_addon.release_date')
    );

    Tygh::$app['view']->assign('selected_fields', $selected_fields);

} elseif ($mode == 'm_update') {

    $selected_fields = Tygh::$app['session']['selected_fields'];

    $field_groups = Tygh::$app['view']->getTemplateVars('field_groups');
    $filled_groups = Tygh::$app['view']->getTemplateVars('filled_groups');
    $field_names = Tygh::$app['view']->getTemplateVars('field_names');

    if (!empty($selected_fields['data']['technical_description'])) {
        $field_groups['B']['technical_description'] = 'products_data';
        $filled_groups['B']['technical_description'] = __('technical_description');
    }
    if (isset($field_names['technical_description'])) {
        unset($field_names['technical_description']);
    }
    if (!empty($selected_fields['data']['release_date'])) {
        $field_groups['B']['release_date'] = 'products_data';
        $filled_groups['B']['release_date'] = __('my_second_addon.release_date');
    }
    if (isset($field_names['release_date'])) {
        unset($field_names['release_date']);
    }
    if (!empty($selected_fields['data']['ean_code'])) {
        $field_groups['B']['ean_code'] = 'products_data';
        $filled_groups['B']['ean_code'] = __('my_second_addon.ean_code');
    }
    if (isset($field_names['ean_code'])) {
        unset($field_names['ean_code']);
    }
    
    Tygh::$app['view']->assign([
        'field_groups' => $field_groups,
        'filled_groups' => $filled_groups,
        'field_names' => $field_names
    ]);
}
